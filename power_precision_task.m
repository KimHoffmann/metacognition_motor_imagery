%Metacognition of motor imagery -power vs. precision grip

clear all;
close all;

user = 'Kim'; %% home purpose
switch user
    case 'Carina'
        root_path = '/scratch/carina/imagery_metacognition/';
        data_path = '/scratch/carina/imagery_metacognition/data/';
        port_arduino = '/dev/ttyACM0';
        labjack_header = '/scratch/carina/labjack/include/labjackusb.h';
        labjack_library = '/scratch/carina/labjack/lib/liblabjackusb';
    case 'Christina'
        root_path = '/scratch/share/Dropbox/HiWis/Christina/imagery_metacognition';
        data_path = '/scratch/share/Dropbox/Data/MotorImagery';
        port_arduino = '/dev/ttyACM0';
        labjack_header = '/home/christina/tools/exodriver/include/labjackusb.h';
        labjack_library = '/home/christina/tools/exodriver/lib/liblabjackusb';
    case 'Marika'
        root_path = '/scratch/Marika/imagery_metacognition/';
        data_path = '/scratch/Marika/imagery_metacognition/data/';
        port_arduino = '/dev/ttyACM0';
        labjack_header = '/scratch/Marika/labjack/include/labjackusb.h';
        labjack_library = '/scratch/Marika/labjack/lib/liblabjackusb';
    case 'Kim'
        root_path = 'C:\Users\kimca\OneDrive\Desktop\motor_imagery_metacognition\metacognition_of_motor_imagery';
        data_path = 'C:\Users\kimca\OneDrive\Desktop\motor_imagery_metacognition\metacognition_of_motor_imagery\data';
        port_arduino = 'COM3';
        
end

addpath([root_path filesep 'functions'])

if ~isfolder(data_path), mkdir(data_path), end

global dev
global cfg
global ard
global lj

%% Get ID and age of the participant with input dialog

% colour_condition = 1: imagine = blue, action = green
prompts = {'ID' 'age' 'colour condition'};

% Specify the default
% These will appear already in the response fields
defaults = {'0' '25' '1'};

% Show the dialog box and store the answers in an object
infoparticipant = inputdlg(prompts, 'Participant information', 1, defaults);

DATA.ID = infoparticipant{1};
DATA.age = infoparticipant{2};
cfg.colour_condition = str2double(infoparticipant{3});

% set subject data path
path_tosave = [data_path filesep DATA.ID];


%% basic settings

commandwindow;

% number of trials should be a number that can be divided through 4
cfg.ntrialsperBlock = 16;
cfg.nblocks = 10;
cfg.ntrainingtrials = 12;


cfg.act    = 1; % act = 0 when arduino is not connected (in action phase press any key)
cfg.emg    = 0; % emg = 0 when labjack is not connected
cfg.rating = 1; % rating = 0: no vividness and effort ratings

cfg.debug  = 1; % debug = 1: short imagining time, only 2 blocks & 4 trials

KbName('UnifyKeyNames');

% colours
cfg.grey = [150 150 150];

cfg.txtCol  = [0 0 0];
cfg.cursorCol = [255 0 0];

if cfg.colour_condition==1 % condition = 1 : imagine = blue, action = green,
    cfg.colour_imagine = [0 0 255];
    cfg.colour_action  = [0 255 0];
else
    cfg.colour_imagine = [0 255 0];
    cfg.colour_action  = [0 0 255];
end

% messages
cfg.language = 'English';
set_Messages();

% mouse
dev.mouseId = GetMouseIndices;
dev.mouseId = dev.mouseId(1);

% Screen
dev.whichscreen = 0;
dev.screeninfo = Screen('Resolution', dev.whichscreen);

if strcmp(user,'Kim') %% change for home purpose
    Screen('Preference', 'SkipSyncTests', 1);
    windowsize = [0 0 dev.screeninfo.width dev.screeninfo.height];
else
    Screen('Preference', 'SkipSyncTests', 0);
    windowsize = [0 0 dev.screeninfo.width dev.screeninfo.height];
end

if cfg.debug
    windowsize = [0 0 200 300];
end 

[dev.wnd, dev.rect] = Screen('OpenWindow', dev.whichscreen, cfg.grey, windowsize);
Screen('BlendFunction', dev.wnd, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA'); % this is needed for transparent background in pictures
%HideCursor(dev.whichscreen);

dev.xCenter = (dev.rect(3) - dev.rect(1))/2 + dev.rect(1);
dev.yCenter = (dev.rect(4) - dev.rect(2))/2 + dev.rect(2);

Screen('TextSize', dev.wnd, 40);
Screen('TextFont', dev.wnd, 'Ariel');

% scale settings
cfg.type2.width_inPixels = 300;
cfg.type2.offset_inPixels = 0;
cfg.type2.arrowWidth_inPixels = 20;
cfg.type2.color = [0 0 0];

cfg.type2.maxRT = Inf; % in secs
cfg.type2.confFBDuration_inSecs = 0.5;

cfg.type2.keys = [KbName('1!') KbName('2@') KbName('3#') KbName('4$')];

% display elements (images and buttons)
set_displayElements();

% other parameters
cfg.proportion_congruent = 0.6;

cfg.cue_time = 1.5;
cfg.imagine_time  = 6;
cfg.wait_time_min = 0.5;
cfg.wait_time_max = 1.5;

cfg.threshCrimp = 100;
cfg.threshSideway = 100;

% parameter changes for debugging
if cfg.debug
    cfg.imagine_time = 1;
    cfg.ntrialsperBlock = 4;
    cfg.nblocks = 2;
    cfg.ntrainingtrials = 4;
    cfg.wait_time_max = 0.5;
    path_tosave = [root_path filesep 'test_data'];
end


%% create subject data folder
if ~isfolder(path_tosave), mkdir(path_tosave), end


%% start arduino sensor
if cfg.act
    if ~isempty(instrfind)
        fclose(instrfind);
        delete(instrfind);
    end
    ard = serial(port_arduino,'Baud',115200);
    ard.InputBufferSize=10000;
    fopen(ard);  % Opening the communication channel takes some time.
    WaitSecs(2); % This ensures it is open before we start to query it.
else
    ard = [];
end


%% start labjack
if cfg.emg
   lj = labJack('deviceID', 3, 'header', labjack_header, 'library', labjack_library);
else
   lj = [];
end


%% trial structure
[DATA.main.action, DATA.main.imagine, DATA.main.prop_congruent] = set_trialstructure(cfg.nblocks, cfg.ntrialsperBlock, cfg.proportion_congruent);
[DATA.train.action, DATA.train.imagine, DATA.train.prop_congruent] = set_trialstructure(1, cfg.ntrainingtrials, cfg.proportion_congruent);


%% start training

Screen('FillRect', dev.wnd,  cfg.grey);
DrawFormattedText(dev.wnd, cfg.messages.stringWelcome, 'center','center', cfg.txtCol);
Screen('Flip', dev.wnd);

RestrictKeysForKbCheck([]);
keyIsDown=0;
while keyIsDown==0
    [keyIsDown,~,~] = KbCheck;
end

DATA.train.time.starttime = GetSecs();
 if cfg.emg, lj.setFIOs([0 0 0 0 0 0 0 0],0:7); lj.setFIOs([1 1 0 0 0 0 0 0],0:7); end % 12 = start training

for n = 1:cfg.ntrainingtrials
    % run a trial
    DATA = trial('train', DATA, 1, n);
end
DATA.train.time.endtime = GetSecs();
 if cfg.emg, lj.setFIOs([0 0 0 0 0 0 0 0],0:7); lj.setFIOs([1 1 0 1 0 0 0 0],0:7); end % 13 = end training

% save training data
saveFilename = [path_tosave filesep 'participant_' num2str(DATA.ID) '_train.mat'];
save(saveFilename,'DATA','cfg','dev')


%% start main part

Screen('FillRect', dev.wnd,  cfg.grey);
DrawFormattedText(dev.wnd, cfg.messages.stringMainPart, 'center','center', cfg.txtCol);
Screen('Flip', dev.wnd);

RestrictKeysForKbCheck(KbName('b'));
keyIsDown=0;
while keyIsDown==0
    [keyIsDown,~,~] = KbCheck;
end

DATA.main.time.starttime = GetSecs();
 if cfg.emg, lj.setFIOs([0 0 0 0 0 0 0 0],0:7); lj.setFIOs([1 1 1 0 0 0 0 0],0:7); end % 14 = start main part

for b = 1:cfg.nblocks
    for n = 1:cfg.ntrialsperBlock
        % run a trial
        DATA = trial('main', DATA, b, n);
    end
    
    % save data for this block
    saveFilename = [path_tosave filesep 'participant_' num2str(DATA.ID) '_block' num2str(b) '.mat'];
    save(saveFilename,'DATA','cfg','dev')
    
    % show end of block screen
    if b~=cfg.nblocks
        DrawFormattedText(dev.wnd, [cfg.messages.stringEndBlock1 num2str(b) cfg.messages.stringEndBlock2 num2str(cfg.nblocks) cfg.messages.stringEndBlock3], 'center', 'center', cfg.txtCol);
    else
        DrawFormattedText(dev.wnd, [cfg.messages.stringEndBlock1 num2str(b) cfg.messages.stringEndBlock2 num2str(cfg.nblocks) cfg.messages.stringEndBlock3_last], 'center', 'center', cfg.txtCol);
    end
    Screen('Flip', dev.wnd);
    RestrictKeysForKbCheck(KbName('b'));
    KbStrokeWait;

end
DATA.main.time.endtime = GetSecs();
 if cfg.emg, lj.setFIOs([0 0 0 0 0 0 0 0],0:7); lj.setFIOs([1 1 1 1 0 0 0 0],0:7); end % 15 = end main part

%% Save data

saveFilename = [path_tosave filesep 'participant_' num2str(DATA.ID) '.mat'];
save(saveFilename,'DATA','cfg','dev')

DrawFormattedText(dev.wnd, cfg.messages.stringEnd, 'center', 'center', cfg.txtCol);
Screen('Flip', dev.wnd);
 
WaitSecs(1);
sca